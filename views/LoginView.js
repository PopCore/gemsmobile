'use strict';

import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
} from 'react-native';

var style = require('../style/style');
var t = require('tcomb-form-native');
let Form = t.form.Form;

// User struct holds the basic data required to
// sign in a user.
var User = t.struct({
  email: t.String,
  password: t.String
});

const formOptions = {
  auto: 'placeholders'
};

export default class LoginView extends Component {
  _userSignup() {
    return fetch('http://0.0.0.0:3000/signin', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/vnd.api+json',
      },
      body: JSON.stringify({
        data: {
          attributes: {
            email: 'test@email.com',
            password: 'password123',
          }
        }
      })
    })
    .then((response) => {
      console.log(response.json())
    })
    .catch((error) => {
      console.log(error);
    })
  }

  render() {
    return (
      <View style={ style.container }>

        <View style={ style.formContainer }>
          <Form
            ref="form"
            type={User}
            options={formOptions}
          />
        </View>

        <View style={ style.buttonsContainer }>
          <TouchableHighlight style={ style.button } onPress={this._userLogin} underlayColor='#99d9f4'>
            <Text style={ style.buttonText }>Login</Text>
          </TouchableHighlight>
          <TouchableHighlight style={ style.button } onPress={ () => this.props.navigator.pop() }>
            <Text style={ style.buttonText }>Back</Text>
          </TouchableHighlight>
        </View>

      </View>
    )
  }
}