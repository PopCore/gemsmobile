'use strict';

import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
} from 'react-native';

var style = require('../style/style');

export default class SignupView extends Component {
  _userSignup() {
    return fetch('http://0.0.0.0:3000/signup', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/vnd.api+json',
      },
      body: JSON.stringify({
        data: {
          attributes: {
            username: 'test',
            email: 'test@email.com',
            password: 'password123',
            city: 'London'
          }
        }
      })
    })
    .then((response) => {
      console.log(response.json())
    })
    .catch((error) => {
      console.log(error);
    })
  }

  render() {
    return (
      <View style={ style.container }>
        <Text style={ style.heading }>Hello</Text>
        <TouchableHighlight style={ style.button } onPress={ () => this.props.navigator.pop() }>
          <Text style={ style.buttonText }>GO Back</Text>
        </TouchableHighlight>
      </View>
    )
  }
}


<View style={styles.formContainer}>
  <Form
    ref="form"
    type={User}
    options={options}
  />
</View>

<View style={styles.buttonsContainer}>
  <TouchableHighlight style={styles.button} onPress={this._navigate('YOYOYOYOYO')} underlayColor='#99d9f4'>
    <Text style={styles.buttonText}>Signup</Text>
  </TouchableHighlight>
  <TouchableHighlight style={styles.button} onPress={this._userLogin} underlayColor='#99d9f4'>
    <Text style={styles.buttonText}>Login</Text>
  </TouchableHighlight>
</View>