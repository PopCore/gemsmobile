'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  styleheet,
  Text,
  View,
  Navigator,
  TouchableHighlight
} from 'react-native';
import LoginView from './views/LoginView'

var style = require('./style/style');

class GemsMobile extends Component {

  renderScene(route, navigator) {
    return <route.component navigator={navigator} {...route.passProps} />
  }

  configureScene(route, routeStack){
    if(route.type == 'Modal') {
      return Navigator.SceneConfigs.FloatFromBottom
    }
    return Navigator.SceneConfigs.PushFromRight
  }

  render() {
    return (

      <Navigator
        configureScene = {this.configureScene}
        style={{ flex:1 }}
        initialRoute={{ component: Main }}
        renderScene={ this.renderScene } /
      >
    )
  }
};

class Main extends Component {
  _navigate(name) {

    switch (name){
      case 'login':
        this.props.navigator.push({
          component: LoginView,
          passProps: {
            name: name
          }
        });
      break;

      case 'signup':
        this.props.navigator.push({
          component: LoginView,
          passProps: {
            name: name
          }
        });
      break;

      default:
        //pass
    }
  }

  render() {
    return(
      <View style={ style.container }>

        <View style={ style.fullWidthBlock }>
          <Text style={ style.heading }>GEMS!</Text>
        </View>

        <View style={ style.buttonsContainer }>
          <TouchableHighlight style={ style.button } onPress={ () => this._navigate('signup') }>
            <Text style={ style.buttonText }>Sign Up</Text>
          </TouchableHighlight>
          <TouchableHighlight style={ style.button } onPress={ () => this._navigate('login') }>
            <Text style={ style.buttonText }>Log In</Text>
          </TouchableHighlight>
        </View>

      </View>
    )
  }
}

AppRegistry.registerComponent('GemsMobile', () => GemsMobile);
